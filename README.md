# Hourly

Hourly is a time logger that makes it easy to record time spent on different projects while working.

Add all projects you are working on in the file projects.txt, one per line. Then, start the application with:

    python hourly.py

Or, if your environment is set up that way:

    python3 hourly.py

Switch which projects you are working on by pressing the corresponding number.

