import os
from datetime import datetime, timedelta

from threading import Thread
from time import sleep
from getkey import getkey


last_update = datetime.now()

time_spent_working = {}
with open("projects.txt") as projects:
    for project in projects:
        time_spent_working[project.strip()] = timedelta(seconds=0)

client_indexes = {}
i = 1
working_index = 1
for key in time_spent_working.keys():
    client_indexes[i] = key
    i += 1


def main_loop():
    global time_spent_working
    global working_index
    global client_indexes
    global last_update
    while True:
        current_client = client_indexes[working_index]
        time_spent_working[current_client] += datetime.now() - last_update
        last_update = datetime.now()
        print_status()
        microsecond_overshoot = int(time_spent_working[current_client].microseconds)
        sleep(1 - (microsecond_overshoot/1_000_000))


def input_loop():
    global working_index
    global last_update
    while True:
        key_input = getkey()
        if not key_input.isnumeric():
            continue
        index = int(key_input)
        if index in client_indexes.keys():
            switch_client(new_index=index)
            print_status()


def print_status():
    client = client_indexes[working_index]
    os.system("clear")
    print(f"Working on: {client}")
    index = 1
    for client, time_spent in time_spent_working.items():
        print(f"[{index}] {client}: \t {str(time_spent).split('.')[0]}")
        index += 1


def switch_client(new_index: int):
    global working_index
    global last_update
    current_client = client_indexes[working_index]
    time_spent_working[current_client] += datetime.now() - last_update
    last_update = datetime.now()
    working_index = new_index


m = Thread(target=main_loop)
m.start()
i = Thread(target=input_loop)
i.start()
